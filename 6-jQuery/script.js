$(document).ready(function () {
    $("tbody a.remove-action").click(function () {
        $(this).closest('tr').remove();
    });

    $("thead a.remove-action").click(function() {
        $('tbody input:checked').closest('tr').remove();     
    });

    $("thead input.selection").change(function() {
        $("tbody input.selection").prop('checked', this.checked);
    });
});
