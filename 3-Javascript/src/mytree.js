const Node = require('./node');

class MyTree {
    constructor(args) {
        this.root = new Node(args);
    }
}

module.exports = MyTree;
