class Node {
    constructor(args = {}, parent = null) {
        this.data = args.data;
        this.__children = [];
        this.__parent = parent;
    }

    addNode(args) {
        const child = new Node(args, this);
        this.__children.push(child);

        return child;
    }

    getNodeAt(index) {
        return this.__children[index] || null;
    }

    children() {
        return this.__children;
    }

    parent() {
        return this.__parent;
    }

    maxDepth() {
        if (this.__children.length === 0) {
            return 0;
        }

        return 1 + Math.max(...this.__children.map(c => c.maxDepth()));
    }
}

module.exports = Node;
