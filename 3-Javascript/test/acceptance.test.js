const MyTree = require('../src/mytree');

// utilizzo la sintassi jest in maniera volutamente errata per riportare comodamente il test della traccia per intero
// per i test singoli, fare riferimento a node.test.js e mytree.test.js
describe('Acceptance test', () => {
    it('executes as expected', () => {
        var tree = new MyTree();
        var firstNode = tree.root.addNode();
        var secondNode = tree.root.addNode();
        firstNode.addNode().addNode({ data: 'leaf1' });
        firstNode.getNodeAt(0).addNode({ data: 'leaf2' });

        expect(tree.root.maxDepth()).toBe(3);
        expect(firstNode.maxDepth()).toBe(2);
        expect(secondNode.maxDepth()).toBe(0);
        expect(firstNode.parent()).toBe(tree.root);
    });
});
