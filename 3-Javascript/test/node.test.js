const Node = require('../src/node');

describe('Node', () => {
    let node;

    beforeEach(() => {
        node = new Node();
    })

    it('can have data', () => {
        const node = new Node({ data: 'node data' });
        expect(node.data).toEqual('node data');
    });

    it('has default undefined data', () => {
        expect(node.data).toBeUndefined();
    })

    describe('.addNode', () => {
        it('adds children correctly', () => {
            node.addNode();
            expect(node.children()).toHaveLength(1);
            expect(node.children()[0]).toBeInstanceOf(Node);
        });

        it('adds children with data', () => {
            node.addNode({ data: 'leaf1' });
            expect(node.children()[0].data).toEqual('leaf1')
        });
    });

    describe('.getNodeAt', () => {
        it('returns child at requested index if present', () => {
            const child1 = node.addNode();
            const child2 = node.addNode();

            expect(node.getNodeAt(0)).toBe(child1);
            expect(node.getNodeAt(1)).toBe(child2);
            expect(node.getNodeAt(2)).toBeNull();
        });
    });

    describe('.parent', () => {
        it('returns parent', () => {
            const child = node.addNode();

            expect(child.parent()).toBe(node);
        })
    });

    describe('.maxDepth', () => {
        it('returns 0 with no children', () => {
            expect(node.maxDepth()).toBe(0);
        })

        it('returns maxDepth correctly', () => {
            node.addNode().addNode();
            node.addNode();
            node.addNode().addNode().addNode().addNode();

            expect(node.maxDepth()).toBe(4);
        });
    });
});
