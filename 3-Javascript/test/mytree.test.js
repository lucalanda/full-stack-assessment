const MyTree = require('../src/mytree');
const Node = require('../src/node');

describe('MyTree', () => {
    it('has a root node with void data', () => {
        const tree = new MyTree();

        expect(tree.root).toBeInstanceOf(Node);
        expect(tree.root.data).toBeUndefined();
    });

    it('builds root node with arguments', () => {
        const tree = new MyTree({ data: 'pluto' });

        expect(tree.root.data).toEqual('pluto');
    });
});