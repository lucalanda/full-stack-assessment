# Funzionalità
* sono incluse tutte le funzionalità richieste, note:
  * la modifica/cancellazione di un articolo è possibile solo dal suo dettaglio
  * la modifica/cancellazione di un articolo è possibile solo se loggati con l'account dell'autore
* è possibile registrarsi o loggarsi con uno dei tre utenti inclusi dal seed:
  * raffaello@mascetti.com
  * giorgio@perozzi.com
  * guido@necchi.com
* per tutti e tre gli utenti la password d'accesso è "password"

# Requisiti
* ruby v2.6.3
* bundler v2.1.4
* yarn

# Setup
* eseguire nella root i comandi:
  * yarn install
  * bundle install
* eseguire il seed del db:
  * rails db:seed

# Esecuzione
* avvio del webserver:
  * comando "rails server" nella root del progetto
* avvio dei test
  * comando "rspec" nella root del progetto
