# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

users_data = [%w[Giorgio Perozzi], %w[Raffaello Mascetti], %w[Guido Necchi]]

users_data.each do |user_data|
  user = User.create!(
      name: user_data[0],
      surname: user_data[1],
      email: "#{user_data[0]}@#{user_data[1]}.com".downcase,
      password: 'password',
      password_confirmation: 'password',
  )

  3.times do
    Article.create!(
        title: Faker::Company.bs,
        content: Faker::Lorem.sentence(word_count: rand(60..100)),
        user: user,
        created_at: Faker::Date.between(from: 4.months.ago, to: Time.now),
        updated_at: Faker::Date.between(from: 1.month.ago, to: Time.now)
    )
  end
end
