Rails.application.routes.draw do
  root 'articles#index'

  get 'logout', to: 'sessions#destroy'
  get 'login', to: 'sessions#new'
  post 'login', to: 'sessions#create'

  resources :users, only: %i[new create]
  resources :articles
end
