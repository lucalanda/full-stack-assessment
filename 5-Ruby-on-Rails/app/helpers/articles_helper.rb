module ArticlesHelper
  def requested_articles
    articles = @selected_user&.articles || Article.all

    articles.order selected_sort_option
  end

  def selected_sort_option
    case @selected_sort&.to_sym
    when :created_desc
      { created_at: :desc }
    when :created_asc
      { created_at: :asc }
    when :updated_desc
      { updated_at: :desc }
    when :updated_asc
      { updated_at: :asc }
    else
      { created_at: :desc }
    end
  end

  def sorting_options
    [
        ['Created: newest', :created_desc],
        ['Created: oldest', :created_asc],
        ['Updated: newest', :updated_desc],
        ['Updated: oldest', :updated_asc]
    ]
  end

  def no_articles_message
    if @selected_user.present?
      "No articles from #{@selected_user.full_name} yet."
    else
      'No articles to display yet.'
    end
  end
end