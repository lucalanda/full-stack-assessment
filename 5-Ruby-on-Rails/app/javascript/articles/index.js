$(document).ready(() => {
    $('#select-author').change(changeQueryParams);
    $('#select-sorting').change(changeQueryParams);
});

const changeQueryParams = () => {
    const userId = $('#select-author').val();
    const sortingOption = $('#select-sorting').val();

    const { origin } = new URL(window.location);
    const queryString = $.param({ user_id: userId, sort: sortingOption });

    window.location.href = `${origin}/?${queryString}`;
};
