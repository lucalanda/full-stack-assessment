class SessionsController < ApplicationController
  def new; end

  def create
    user = User.find_by_email(params[:email])
    if user&.authenticate(params[:password])
      log_in user
      redirect_to root_url, notice: "Logged in as #{user.name} #{user.surname}!"
    else
      flash.now[:alert] = 'Email or password is invalid'
      render :new
    end
  end

  def destroy
    log_out if logged_in?
    redirect_to root_url, notice: 'Logged out!'
  end
end
