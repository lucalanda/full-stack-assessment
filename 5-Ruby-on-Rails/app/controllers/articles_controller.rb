class ArticlesController < ApplicationController
  include ArticlesHelper

  before_action :set_article, only: %i[show edit update destroy]
  before_action :article_exists?, only: %i[show edit update destroy]
  before_action :correct_user, only: %i[edit update destroy]
  before_action :is_logged_in?, only: %i[new create]

  def index
    @selected_user = User.find_by_id(params[:user_id])
    @selected_sort = params[:sort]
    @articles = requested_articles
  end

  def show; end

  def new
    @article = Article.new
  end

  def edit; end

  def create
    @article = Article.new(article_params)

    if @article.save
      redirect_to @article, notice: 'Article was successfully created.'
    else
      render :new
    end
  end

  def update
    if @article.update(article_params)
      redirect_to @article, notice: 'Article was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @article.destroy
    redirect_to articles_url, notice: 'Article was successfully deleted.'
  end

  private

  def set_article
    @article = Article.find_by(id: params[:id])
  end

  def is_logged_in?
    redirect_to root_path, alert: 'You must log in to create an article' unless logged_in?
  end

  def article_exists?
    redirect_to root_path, alert: 'The requested article does not exist' unless @article.present?
  end

  def correct_user
    redirect_to root_path, alert: 'You can only edit or delete your own articles.' unless @article.user == current_user
  end

  def article_params
    params.require(:article).permit(:title, :content).merge({ user: current_user })
  end
end
