require_relative '../rails_helper'

describe Article do
  fixtures :users

  before do
    user = users(:guido)
    @article = Article.new(title: "Mario", content: "Rossi", user: user)
  end

  it 'should be valid' do
    expect(@article).to be_valid
  end

  it 'must have a title' do
    @article.title = ''
    expect(@article).not_to be_valid
  end

  it 'must have a title shorter than 50' do
    @article.title = 'c' * 51
    expect(@article).not_to be_valid
  end

  it 'must have a content' do
    @article.content = ''
    expect(@article).not_to be_valid
  end

  it 'must have a valid user' do
    @article.user_id = 'unexisting id'
    expect(@article).not_to be_valid

    @article.user_id = nil
    expect(@article).not_to be_valid
  end
end