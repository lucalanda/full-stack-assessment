require_relative '../rails_helper'

describe User do
  before do
    @user = User.new(name: "Mario", surname: "Rossi", email: "user@example.com",
                     password: "foobar", password_confirmation: "foobar")
  end

  it 'should be valid' do
    expect(@user).to be_valid
  end

  it 'must have a name' do
    @user.name = ''
    expect(@user).not_to be_valid
  end

  it 'must have a surname' do
    @user.surname = ''
    expect(@user).not_to be_valid
  end

  it 'must have an email' do
    @user.email = ''
    expect(@user).not_to be_valid
  end

  it 'must have a valid email' do
    @user.email = 'non@valid email.'
    expect(@user).not_to be_valid
  end

  it 'must have a password' do
    @user.password = ''
    @user.password_confirmation = ''
    expect(@user).not_to be_valid
  end

  it 'deletes own articlces on destroy' do
    @user.save!
    Article.create! title: 'title', content: 'content', user: @user

    expect { @user.destroy }.to change { Article.count }.by -1
  end
end