require_relative '../rails_helper'

describe UsersController do
  fixtures :users

  describe '/new' do
    it 'renders correctly' do
      get :new
      expect(response).to be_successful
    end
  end

  describe '/create' do
    before do
      @user_params = {
          name: 'mario',
          surname: 'rossi',
          email: 'test@email.it',
          password: 'password',
          password_confirmation: 'password'
      }
    end

    context 'with wrong data' do
      it 'does not create user' do
        wrong_create_params = @user_params.merge({ name: nil })

        expect { post :create, params: { user: wrong_create_params } }.not_to change { User.count }
      end
    end

    context 'with correct data' do
      it 'logs in and redirects to root' do
        post :create, params: { user: @user_params }

        expect(logged_in?).to be_truthy
        expect(response).to redirect_to root_path
      end
    end
  end
end