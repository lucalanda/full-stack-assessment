require_relative '../rails_helper'

describe SessionsController do
  fixtures :users

  before { @user_guido = users(:guido) }

  describe '/new' do
    it 'should respond correctly' do
      get :new
      expect(response).to be_successful
    end
  end

  describe '/create' do
    before do
      mock_flash
    end

    context 'with wrong email' do
      it 'should render new' do
        post :create, params: { email: 'unexisting', password: 'wrong password' }
        expect(response).to be_successful
        expect(@flash_args[:alert]).to be_present
      end
    end

    context 'with wrong password' do
      it 'should render new' do
        post :create, params: { email: @user_guido.email, password: 'wrong password' }
        expect(response).to be_successful
        expect(@flash_args[:alert]).to be_present
      end
    end

    context 'with correct data' do
      it 'should redirect to root url' do
        post :create, params: { email: @user_guido.email, password: 'password' }
        expect(response).to redirect_to root_path
        expect(logged_in_as?(@user_guido)).to be_truthy
      end
    end
  end

  describe '/destroy' do
    it 'should log out and redirect to root' do
      login_as @user_guido
      delete :destroy
      expect(response).to redirect_to root_path
      expect(logged_in?).to be_falsey
    end
  end
end