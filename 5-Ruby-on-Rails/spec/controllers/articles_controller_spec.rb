require_relative '../rails_helper'

describe ArticlesController do
  fixtures :users, :articles

  before do
    @article_post_params = { article: { title: 'My article', content: 'some good content' } }
    @user_guido = users(:guido)
    @user_raffaello = users(:raffaello)
  end

  describe '/new' do
    context 'when logged in' do
      it 'renders correctly' do
        login_as @user_guido

        get :new

        expect(response).to be_successful
      end
    end

    context 'when not logged in' do
      it 'redirects to root' do
        logout

        get :new

        expect(response).to redirect_to root_path
      end
    end
  end

  describe '/show' do
    context 'when article does not exist' do
      it 'redirects to root' do
        get :show, params: { id: 'unexisting id' }

        expect(response).to redirect_to root_path
      end
    end

    context 'when article exists' do
      it 'renders correctly' do
        get :show, params: { id: @user_guido.articles.first.id }

        expect(response).to be_successful
      end
    end
  end

  describe '/create' do
    context 'when not logged in' do
      it 'redirects to root' do
        logout

        post :create, params: @article_post_params

        expect(response).to redirect_to root_path
      end
    end

    context 'when logged in' do
      it 'creates article correctly' do
        login_as @user_guido

        expect { post :create, { params: @article_post_params } }.to change { @user_guido.articles.count }.by 1
      end
    end
  end

  describe '/edit' do
    before { login_as @user_guido }

    context 'when user is not the author' do
      it 'redirects to root' do
        get :edit, params: { id: @user_raffaello.articles.first.id }

        expect(response).to redirect_to root_path
      end
    end

    context 'when article does not exist' do
      it 'redirects to root' do
        get :edit, params: { id: 'unexisting id' }

        expect(response).to redirect_to root_path
      end
    end

    context 'when user is the author' do
      it 'renders correctly' do
        get :edit, params: { id: @user_guido.articles.first.id }

        expect(response).to be_successful
      end
    end
  end

  describe '/update' do
    before { login_as @user_guido }

    context 'when user is not the author' do
      it 'redirects to root' do
        post :update, params: { id: @user_raffaello.articles.first.id }.merge(@article_post_params)

        expect(response).to redirect_to root_path
      end
    end

    context 'when article does not exist' do
      it 'redirects to root' do
        post :update, params: { id: 'unexisting id' }.merge(@article_post_params)

        expect(response).to redirect_to root_path
      end
    end

    context 'when user is the author' do
      it 'updates article correctly' do
        post :update, params: { id: @user_guido.articles.first.id, article: { title: 'new title' } }

        expect(@user_guido.articles.first.title).to eq 'new title'
      end
    end
  end

  describe '/destroy' do
    before { login_as @user_guido }

    context 'when not logged in' do
      it 'redirects to root' do
        logout

        post :destroy, params: { id: @user_guido.articles.first.id }

        expect(response).to redirect_to root_path
      end
    end

    context 'when user is not the author' do
      it 'redirects to root' do
        post :destroy, params: { id: @user_raffaello.articles.first.id }

        expect(response).to redirect_to root_path
      end
    end

    context 'when article does not exist' do
      it 'redirects to root' do
        post :destroy, params: { id: 'unexisting id' }

        expect(response).to redirect_to root_path
      end
    end

    context 'when user is the author' do
      it 'destroys article correctly' do
        destroy_params = { id: @user_guido.articles.first.id }
        expect { post :destroy, params: destroy_params }.to change { @user_guido.articles.count }.by -1
      end
    end
  end
end