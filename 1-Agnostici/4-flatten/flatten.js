const flatten = (array) => {
    const result = [];

    array.forEach((element) => {
        if (element instanceof Array) {
            result.push(...flatten(element));
        } else {
            result.push(element);
        }
    });

    return result;
};

module.exports = flatten;
