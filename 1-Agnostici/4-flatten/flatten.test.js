const flatten = require('./flatten');

describe('.flatten', () => {
    it('flattens nested arrays correctly', () => {
        const array = [1, [4, 5], [2, [3, 4]]];

        expect(flatten(array)).toEqual([1, 4, 5, 2, 3, 4]);
    })
});
