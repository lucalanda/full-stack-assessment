const axios = require('axios');

const getHumans = async () => {
    const baseURL = 'https://ghibliapi.herokuapp.com/';

    const speciesResponse = await axios.get('/species', {
        baseURL,
        params: { name: 'Human' },
    });

    const humanSpeciesURL = speciesResponse.data[0].url;

    const peopleResponse = await axios.get('/people', {
        baseURL,
        params: { species: humanSpeciesURL },
    });

    return peopleResponse.data;
}

const main = async () => {
    const people = await getHumans();

    console.log(people.map(person => person.name));
}

main();
