# Spiegazione
L'algoritmo BubbleSort implementato effettua al più N scansioni da sinistra a destra, comparando coppie adiacenti di elementi.

Quando nel confronto verifica che i due elementi non sono ordinati (nel nostro caso in maniera ascendente), li scambia.

La versione implementata contiene un'ottimizzazione, basata sull'assunto che se una scansione viene completata senza alcuno scambio 
allora l'array è già completamente ordinato. Quando ciò si verifica, quest'implementazione interrompe l'esecuzione.

L'algoritmo è notoriamente molto meno efficiente delle usuali implementazioni di default usate dai linguaggi (es. mergesort e quicksort),
rappresenta per lo più un esempio base.

# Complessità temporale
* O(n^2), nel caso in cui l'array sia in ordine inverso a quello desiderato (per questo esercizio, in caso sia in ordine discendnete)
* o(n), nel caso in cui l'array sia già ordinato nel verso desiderato
* non ha senso considerare un caso "medio" di riferimento per questo tipo di algoritmo

# Complessità spaziale
* O(1), perchè è richiesta solo una variabile ausiliaria tmp durante lo scambio