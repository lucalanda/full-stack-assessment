const bubbleSort = (compare) => (array) => {
    for (let i = 0; i <= array.length; i++) {
        let changed = false;

        for (let j = 0; j < array.length - 1; j++) {
            if (compare(array[j], array[j + 1]) < 0) {
                swapElements(array, j, j + 1);
                changed = true;
            }
        }

        if (!changed) break;
    }

    return array;
};

const swapElements = (array, i, j) => {
    let tmp = array[i];
    array[i] = array[j];
    array[j] = tmp;
}

const sort = bubbleSort((a, b) => b - a);

module.exports = sort;
