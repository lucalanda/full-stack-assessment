const sort = require('./sort');

describe('.sort', () => {
    it('sorts array ascending', () => {
        const array1 = [2, 1, 4, 6, 4, 5];
        const array2 = [1, 2, 4, 7];
        const array3 = [4, 1, 3, 3, 2, 3, 3, 5, 9, 0];

        expect(sort(array1)).toEqual([1, 2, 4, 4, 5, 6]);
        expect(sort(array2)).toEqual([1, 2, 4, 7]);
        expect(sort(array3)).toEqual([0, 1, 2, 3, 3, 3, 3, 4, 5, 9]);
    })
})