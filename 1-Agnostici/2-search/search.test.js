const search = require('./search');

describe('.search', () => {
    it('returns -1 if the occurrence requested was not found', () => {
        const array = ['pippo', 'pluto', 'paperino'];

        expect(search(array, 'pippo')).toBe(0);
        expect(search(array, 'paperino')).toBe(2);
    });

    it('returns the index of first occurrence requested', () => {
        const array = ['pippo', 'pluto', 'paperino'];

        expect(search(array, 'topolino')).toBe(-1);
    });
});
