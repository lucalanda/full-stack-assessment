select line_items.order_id
from line_items join products on line_items.product_id = products.id
where sku like 'G%'
group by line_items.order_id
having (count(products.id) > 1)