select distinct customers.id
from customers
inner join orders on orders.customer_id = customers.id
inner join line_items on line_items.order_id = orders.id
inner join products on products.id = line_items.product_id
where products.sku = 'X_Y_Z'
